package datacreator.abstraction;

import java.util.ArrayList;
import java.util.List;

public class Line {

	private List<Cell> cells;

	public List<Cell> getCells() {
		return cells;
	}

	public void setCells(List<Cell> cells) {
		this.cells = cells;
	}

	public void addCell(Cell cell) {
		if(cells == null)
			cells = new ArrayList<Cell>();
		
		cells.add(cell);
	}
	
}
