package datacreator.abstraction;

public class Cell {
	
	public String value;

	public Cell(String value) {
		this.value = value;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

}
