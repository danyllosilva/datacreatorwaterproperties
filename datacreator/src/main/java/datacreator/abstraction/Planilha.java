package datacreator.abstraction;

import java.util.ArrayList;
import java.util.List;

public class Planilha {
	
	private Line header;
	private List<Line> lines;

	public List<Line> getLines() {
		return lines;
	}

	public void setLines(List<Line> lines) {
		this.lines = lines;
	}

	public Line getHeader() {
		return header;
	}

	public void setHeader(Line header) {
		this.header = header;
	}

	public void addLine(Line line) {
		if(lines == null)
			lines = new ArrayList<Line>();
		
		lines.add(line);
	}
	

}
