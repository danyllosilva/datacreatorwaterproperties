package datacreator;

import java.time.LocalDateTime;
import java.time.temporal.TemporalAdjusters;

import datacreator.build.DataBuilder;
import datacreator.csv.CSVCreator;

public class AllYearUntilNow {
	
	public static void main(String[] args) throws InterruptedException {


		LocalDateTime lastMonth = LocalDateTime.now().withMonth(7).withHour(0).withMinute(0).withSecond(0);
		
		LocalDateTime now = lastMonth.with(TemporalAdjusters.firstDayOfMonth());
		LocalDateTime lastDay = now.plusMonths(1);
		
		int index = 0;
		
		while(! now.equals(lastDay)){
			
			String fileName = "/var/camarao/dataset/File" + index + ".csv";
			CSVCreator.create(DataBuilder.build(now), fileName);

			System.out.println("done " + index + "!");
			now = now.plusHours(1);
			
			index ++;
		}
		
		System.out.println("finished!");
	}
	
}