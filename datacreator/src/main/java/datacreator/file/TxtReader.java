package datacreator.file;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class TxtReader {

	public static List<String> openFile(String fileName) {
		List<String> list  = new ArrayList<String>();
		
		try {
			FileReader arq = new FileReader(fileName);
			BufferedReader lerArq = new BufferedReader(arq);

			// lê a primeira linha a variável "linha" recebe o valor "null" quando o processo de repetição atingir o final do arquivo texto
			String linha = lerArq.readLine(); 
			
			while (linha != null) {
				list.add(linha);
				linha = lerArq.readLine(); // lê da segunda até a última linha
			}

			arq.close();
		}
		catch (IOException e) {
			System.err.printf("Erro na abertura do arquivo: %s.\n", e.getMessage());
		}
		
		return list;
	}
}