package datacreator;

import java.time.DayOfWeek;
import java.time.LocalDateTime;

import datacreator.build.DataBuilder;
import datacreator.csv.CSVCreator;

public class Main {
	
	public static void main(String[] args) throws InterruptedException {
		
		LocalDateTime date = LocalDateTime.now().with(DayOfWeek.MONDAY).withHour(0).withMinute(0).withSecond(0);
		
		int delay = Integer.parseInt(args[0]); 
		int numberOfFiles = Integer.parseInt(args[1]);
		
		for (int index = 0; index < numberOfFiles; index++) {
			String fileName = "/var/camarao/data_stream/File" + index + ".csv";
			CSVCreator.create(DataBuilder.build(date), fileName);

			System.out.println("done " + index + "!");
			date = date.plusHours(1);
			
			Thread.sleep(delay);
		}
	}
	
}