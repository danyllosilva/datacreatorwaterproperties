package datacreator;

import java.time.LocalDateTime;
import java.time.temporal.TemporalAdjusters;

import datacreator.build.DataBuilder;
import datacreator.csv.CSVCreator;

public class LastMonthUltilBeginCurrent {
	
	public static void main(String[] args) throws InterruptedException {
		
		LocalDateTime lastMonth = LocalDateTime.now().withMonth(1).withHour(0).withMinute(0).withSecond(0);
		LocalDateTime date = lastMonth.with(TemporalAdjusters.firstDayOfMonth());
		
		int delay = Integer.parseInt(args[0]); 
		int numberOfFiles = Integer.parseInt(args[1]);
		
		for (int index = 0; index < numberOfFiles; index++) {
			String fileName = "/var/camarao/dataset/File" + index + ".csv";
			CSVCreator.create(DataBuilder.build(date), fileName);

			System.out.println("done " + index + "!");
			date = date.plusHours(1);
			
			Thread.sleep(delay);
		}
	}
	
}