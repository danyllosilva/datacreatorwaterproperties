package datacreator.model;

public enum MeasurementType {

	surface, background, half_water;

	public static MeasurementType getEnum(int index){
		return MeasurementType.values()[index];
	}
}
