package datacreator.model;

public enum Propertie {

	salinity("ppt", "0.5", "40", Double.class), 
	temperature("ºC", "26", "33", Integer.class), 
	transparency("cm", "20", "40", Integer.class), 
	organic_matter("mg/L", "20", "40", Integer.class), 
	dissolved_oxygen("", "4", "6.5", Double.class), 
	ph("", "7.5", "8.8", Double.class), 
	alkalinity("", "100", "220", Integer.class), 
	total_hardness("", "100", "2250", Integer.class), 
	total_ammonia_nh3("mg/L", "0.10", "1", Double.class), 
	nitrite_no2("cm", "0", "6", Double.class), 
	nitrate("", "0", "20", Double.class),  
	h2s("mg/L", "0", "1", Double.class), 
	silicate("mg/L", "1", "20", Double.class);

	private String measure;
	private String min;
	private String max;
	private Class<?> type;

	Propertie (String measure, String min, String max, Class<?> type){
		this.measure = measure;
		this.min = min;
		this.max = max;
		this.type = type;
	}
	
	public static Propertie getEnum(int index){
		return Propertie.values()[index];
	}

	public String getMeasure() {
		return measure;
	}

	public void setMeasure(String measure) {
		this.measure = measure;
	}

	public String getMin() {
		return min;
	}

	public void setMin(String min) {
		this.min = min;
	}

	public String getMax() {
		return max;
	}

	public void setMax(String max) {
		this.max = max;
	}

	public Class<?> getType() {
		return type;
	}

	public void setType(Class<?> type) {
		this.type = type;
	}
	
}
