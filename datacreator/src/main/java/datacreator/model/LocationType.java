package datacreator.model;

public enum LocationType {

	tank, lake, box, aquarium;

	public static LocationType getEnum(int index){
		return LocationType.values()[index];
	}
}