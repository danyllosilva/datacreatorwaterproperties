package datacreator.model;

public enum WaterType {

	sweet, salty, brackish;

	public static WaterType getEnum(int index){
		return WaterType.values()[index];
	}
}