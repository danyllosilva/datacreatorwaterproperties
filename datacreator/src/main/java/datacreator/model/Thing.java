package datacreator.model;

import java.util.ArrayList;
import java.util.List;

public class Thing {

	private MeasurementType measurementType;
	private WaterType waterType;
	private LocationType typeLocation;
	private Integer idLocation;
	private Geolocalization geolocalization;
	private List<Propertie> properties = new ArrayList<Propertie>();

	public MeasurementType getMeasurementType() {
		return measurementType;
	}

	public void setMeasurementType(MeasurementType measurementType) {
		this.measurementType = measurementType;
	}

	public WaterType getWaterType() {
		return waterType;
	}

	public void setWaterType(WaterType waterType) {
		this.waterType = waterType;
	}

	public LocationType getTypeLocation() {
		return typeLocation;
	}

	public void setTypeLocation(LocationType typeLocation) {
		this.typeLocation = typeLocation;
	}

	public Integer getIdLocation() {
		return idLocation;
	}

	public void setIdLocation(Integer idLocation) {
		this.idLocation = idLocation;
	}

	public Geolocalization getGeolocalization() {
		return geolocalization;
	}

	public void setGeolocalization(Geolocalization geolocalization) {
		this.geolocalization = geolocalization;
	}

	public List<Propertie> getProperties() {
		return properties;
	}

	public void setProperties(List<Propertie> properties) {
		this.properties = properties;
	}

	public void addPropertie(Propertie propertie) {
		this.properties.add(propertie);
	}

	public String getLocation() {
		return this.typeLocation.toString() + " #" + this.idLocation;
	}

}