package datacreator.csv;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;

import datacreator.abstraction.Cell;
import datacreator.abstraction.Line;
import datacreator.abstraction.Planilha;

public class CSVCreator {

	public static void create(Planilha plan, String fileName) {

		try {
			PrintWriter pw = new PrintWriter(new File(fileName));

			StringBuilder sb = new StringBuilder();

			for (Cell cell : plan.getHeader().getCells()) {
				sb.append(cell.getValue());
				sb.append(',');
			}

			sb.append('\n');

			for (Line line : plan.getLines()) {
				for (Cell cell : line.getCells()) {
					sb.append(cell.getValue());
					sb.append(',');
				}

				sb.append('\n');
			}

			pw.write(sb.toString());
			pw.close();
		} 
		catch (FileNotFoundException e) {
			System.out.println("Cannot create CSV");
			e.printStackTrace();
		}

	}

}