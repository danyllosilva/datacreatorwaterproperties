package datacreator.build;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import datacreator.file.TxtReader;
import datacreator.model.Thing;
import datacreator.model.User;

public class UserBuilder {

	private static final String USERS_CSV = "users.csv";

	private static final int MAX_USERS = 5;
	
	private static List<User> users;
	public static Random gerador = new Random();
	
	public static List<User> generateUsers(){
		if(users == null)
			recuperarUsers();
		
		return users;
	}
	
	private static void recuperarUsers() {
		users = new ArrayList<User>();
		
		List<String> list = TxtReader.openFile(USERS_CSV);
		list = generateRandomList(list);
		
		for (String str : list) {
			String[] arr = str.split(",");
			
			User user = new User();
			user.setId(arr[0]);
			user.setFirstName(arr[1]);
			user.setLastName(arr[2]);
			user.setEmail(arr[3]);
			user.setGender(arr[4]);
			user.setIpAddress(arr[5]);
			
			int randomThings = gerador.nextInt(4);
			int randomProperties = gerador.nextInt(5);
			
			if(randomThings < 3)
				randomThings = 3;
			
			if(randomProperties < 3)
				randomProperties = 3;
			
			List<Thing> things = ThingBuilder.generateThings(randomThings, randomProperties);
			user.setThings(things);
			
			users.add(user);
		}
	}

	private static List<String> generateRandomList(List<String> list) {
		Random random = new Random();
		List<String> other = new ArrayList<String>();
		
		for (int i = 0; i < MAX_USERS; i++) {
			int index = random.nextInt(list.size());
			other.add(list.get(index));
		}
		
		return other;
	}

	public static List<User> getUsers() {
		return users;
	}

	public static void setUsers(List<User> users) {
		UserBuilder.users = users;
	}

}