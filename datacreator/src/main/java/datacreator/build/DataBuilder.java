package datacreator.build;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;
import java.util.Random;

import datacreator.abstraction.Cell;
import datacreator.abstraction.Line;
import datacreator.abstraction.Planilha;
import datacreator.model.Propertie;
import datacreator.model.Thing;
import datacreator.model.User;

public class DataBuilder {

	public static SimpleDateFormat formatador = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	public static Random gerador = new Random();
	
	public static Planilha build(LocalDateTime date){
		
		List<User> users = UserBuilder.generateUsers();
		
		Planilha planilha = new Planilha();
		
		planilha.setHeader(createHeader());
			
		for (int minutes = 0; minutes < 60; minutes++) {
			
			for (int seconds = 0; seconds < 60; seconds++) {
				generateReadTimestamp(users, planilha, date);
				date = date.plusSeconds(1);
			}
		}
		
		return planilha;
	}

	private static void generateReadTimestamp(List<User> users, Planilha planilha, LocalDateTime date) {
		String timeStamp = getNowFormatted(date);
		
		for (User user : users) {
			for (Thing thing : user.getThings()) {
				for (Propertie propertie : thing.getProperties()) {
					createLine(planilha, timeStamp, user, thing, propertie);
				}
			}
		}
	}

	private static void createLine(Planilha planilha, String time_stamp, User user, Thing thing, Propertie propertie) {
		Line line = new Line();
		
		Cell cell = new Cell(time_stamp);
		line.addCell(cell);
		
		cell = new Cell(propertie.toString());
		line.addCell(cell);
		
		String value = "";
		
		if(propertie.getType() == Integer.class){
			value = String.valueOf(randomInteger(propertie.getMin(), propertie.getMax()));
		}
		else if(propertie.getType() == Double.class){
			value = String.valueOf(randomDouble(propertie.getMin(), propertie.getMax()));
		}
		
		cell = new Cell(value);
		line.addCell(cell);

		cell = new Cell(propertie.getMeasure());
		line.addCell(cell);
		
		cell = new Cell(thing.getMeasurementType().toString());
		line.addCell(cell);
		
		cell = new Cell(thing.getWaterType().toString());
		line.addCell(cell);
		
		cell = new Cell(thing.getLocation());
		line.addCell(cell);
		
		cell = new Cell(thing.getGeolocalization().getLatitude());
		line.addCell(cell);
		
		cell = new Cell(thing.getGeolocalization().getLongitude());
		line.addCell(cell);
		
		cell = new Cell(user.getId());
		line.addCell(cell);
		
		cell = new Cell(user.getFirstName() + " " + user.getLastName());
		line.addCell(cell);
		
		cell = new Cell(user.getEmail());
		line.addCell(cell);
		
		cell = new Cell(user.getGender());
		line.addCell(cell);

		cell = new Cell(user.getIpAddress());
		line.addCell(cell);
		
		planilha.addLine(line);
	}
	
	public static int randomInteger(String  minS, String maxS) {
		int min = Integer.parseInt(minS);
		int max = Integer.parseInt(maxS);
		
		return (int) (Math.random() * (max - min)) + min;
	}

	public static double randomDouble(String  minS, String maxS) {
		double min = Double.parseDouble(minS);
		double max = Double.parseDouble(maxS);
		
		double random = (double) (Math.random() * (max - min)) + min;
		DecimalFormat formato = new DecimalFormat("#,#");      
		return Double.valueOf(formato.format(random));
	}

	private static String getNowFormatted(LocalDateTime data) {
		return formatador.format(convertToDateViaSqlTimestamp(data));
	}

	private static Line createHeader() {
		Line header = new Line();
		
		Cell cell = new Cell("time_stamp");
		header.addCell(cell);
		
		cell = new Cell("propertie");
		header.addCell(cell);
		
		cell = new Cell("value");
		header.addCell(cell);

		cell = new Cell("measure");
		header.addCell(cell);
		
		cell = new Cell("measurement_type");
		header.addCell(cell);
		
		cell = new Cell("water_type");
		header.addCell(cell);
		
		cell = new Cell("location");
		header.addCell(cell);
		
		cell = new Cell("latitude");
		header.addCell(cell);
		
		cell = new Cell("longitude");
		header.addCell(cell);
		
		cell = new Cell("idUser");
		header.addCell(cell);
		
		cell = new Cell("responsible");
		header.addCell(cell);
		
		cell = new Cell("email");
		header.addCell(cell);
		
		cell = new Cell("gender");
		header.addCell(cell);

		cell = new Cell("ipAddress");
		header.addCell(cell);
		
		return header;
	}
	
	public static Date convertToDateViaSqlTimestamp(LocalDateTime dateToConvert) {
	    return java.sql.Timestamp.valueOf(dateToConvert);
	}
}
