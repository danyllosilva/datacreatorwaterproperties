package datacreator.build;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import datacreator.file.TxtReader;
import datacreator.model.Geolocalization;

public class GeolocalizationBuilder {

	//http://www.geomidpoint.com/random/
	private static final String GEOLOCALIZATION_TXT = "geolocalization.txt";
	
	private static List<Geolocalization> geolocalizations;
	public static Random gerador = new Random();
	
	public static Geolocalization generateRandomGeolocalizacao(){
		if(geolocalizations == null)
			recuperarGeolocalizations();
		
		int randomGeo = gerador.nextInt(geolocalizations.size());
		Geolocalization geolocalization = geolocalizations.get(randomGeo);
		
		return geolocalization;
	}
	
	private static void recuperarGeolocalizations() {
		geolocalizations = new ArrayList<Geolocalization>();
		
		List<String> list = TxtReader.openFile(GEOLOCALIZATION_TXT);
		
		for (String str : list) {
			String[] arr = str.split(",");
			
			Geolocalization geo = new Geolocalization();
			geo.setLocal1(arr[0]);
			geo.setLatitude(arr[1]);
			geo.setLocal2(arr[2]);
			geo.setLongitude(arr[3]);
			
			geolocalizations.add(geo);
		}
	}

	public static List<Geolocalization> getGeolocalizations() {
		return geolocalizations;
	}

	public static void setGeolocalizations(List<Geolocalization> geolocalizations) {
		GeolocalizationBuilder.geolocalizations = geolocalizations;
	}
	
}
