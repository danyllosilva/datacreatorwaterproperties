package datacreator.build;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import datacreator.model.Geolocalization;
import datacreator.model.LocationType;
import datacreator.model.MeasurementType;
import datacreator.model.Propertie;
import datacreator.model.Thing;
import datacreator.model.WaterType;

public class ThingBuilder {

	public static Random gerador = new Random();

	public static List<Thing> generateThings(int randomThings, int randomProperties) {
		List<Thing> things = new ArrayList<Thing>();

		for (int i = 0; i <= randomThings; i++) {
			Thing thing = new Thing();
			
			for (int j = 0; j < randomProperties; j++) {
				int randomPropertie = gerador.nextInt(Propertie.values().length);
				Propertie propertie = Propertie.getEnum(randomPropertie);
				thing.addPropertie(propertie);
			}

			int randomMeasurementType = gerador.nextInt(MeasurementType.values().length);
			MeasurementType measurementType = MeasurementType.getEnum(randomMeasurementType);
			thing.setMeasurementType(measurementType);

			int randomWaterType = gerador.nextInt(WaterType.values().length);
			WaterType waterType = WaterType.getEnum(randomWaterType);
			thing.setWaterType(waterType);

			int randomTypeLocation = gerador.nextInt(LocationType.values().length);
			LocationType typeLocation = LocationType.getEnum(randomTypeLocation);
			thing.setTypeLocation(typeLocation);
			
			int randomLocation = gerador.nextInt(randomProperties) + 1;
			thing.setIdLocation(randomLocation);
			
			Geolocalization geo = GeolocalizationBuilder.generateRandomGeolocalizacao();
			thing.setGeolocalization(geo);
			
			things.add(thing);
		}

		return things;
	}

}